using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repository.Interface;
using Microsoft.EntityFrameworkCore;


namespace tech_test_payment_api.Repository
{
  public class VendedorRepository : IVendedorRepository
  {
    private readonly VendaContext _context;
    public VendedorRepository(VendaContext vendacontext)
    {
      _context = vendacontext;
    }
    public async Task<Vendedor> Adicionar(Vendedor vendedor)
    {
      await _context.Vendedores.AddAsync(vendedor);
      await _context.SaveChangesAsync();

      return vendedor;
    }

    public async Task<Vendedor> Atualizar(Vendedor vendedor, int id)
    {
      Vendedor vendedorPorId = await BuscarPorId(id);

      if (vendedorPorId == null)
      {
        throw new Exception($"Não existe nenhum Vendedor com Id: {id}. Verifique se digitou corretamente.");
      }

      vendedorPorId.Nome = vendedor.Nome;
      vendedorPorId.Email = vendedor.Email;
      vendedorPorId.CPF = vendedor.CPF;
      vendedorPorId.Telefone = vendedor.Telefone;


      _context.Vendedores.Update(vendedorPorId);
      await _context.SaveChangesAsync();

      return vendedorPorId;
    }

    public async Task<Vendedor> BuscarPorId(int id)
    {
      return await _context.Vendedores.FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<List<Vendedor>> BuscarVendedores()
    {
      return await _context.Vendedores.ToListAsync();
    }
  }
}