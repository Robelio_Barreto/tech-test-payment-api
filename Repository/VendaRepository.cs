using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repository.Interface;
using Microsoft.EntityFrameworkCore;


namespace tech_test_payment_api.Repository
{
  public class VendaRepository : IVendaRepository
  {
    private readonly VendaContext _context;
    public VendaRepository(VendaContext vendacontext)
    {
      _context = vendacontext;
    }
    public async Task<Venda> Adicionar(Venda venda)
    {
      await _context.Vendas.AddAsync(venda);
      await _context.SaveChangesAsync();

      return venda;
    }

    public async Task<Venda> Atualizar(Venda venda, int id)
    {
      Venda vendaPorId = await BuscarPorId(id);

      if (vendaPorId == null)
      {
        throw new Exception($"Não foi encontrado nenhuma venda para o Id : {id} .");
      }

      vendaPorId.Itens = venda.Itens;
      vendaPorId.Valor = venda.Valor;
      vendaPorId.DataVenda = venda.DataVenda;
      vendaPorId.status = venda.status;

      _context.Vendas.Update(vendaPorId);
      await _context.SaveChangesAsync();

      return vendaPorId;
    }

    public async Task<Venda> BuscarPorId(int id)
    {
      return await _context.Vendas
      .Include(x => x.Vendedor)
      .FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<List<Venda>> BuscarVendas()
    {
      return await _context.Vendas
      .Include(x => x.Vendedor)
      .ToListAsync();
    }
  }
}