using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repository.Interface
{
  public interface IVendedorRepository
  {
    Task<Vendedor> BuscarPorId(int id);

    Task<Vendedor> Adicionar(Vendedor vendedor);
    Task<List<Vendedor>> BuscarVendedores();

    Task<Vendedor> Atualizar(Vendedor vendedor, int id);
  }
}