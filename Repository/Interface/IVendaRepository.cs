using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repository.Interface
{
  public interface IVendaRepository
  {
    Task<List<Venda>> BuscarVendas();

    Task<Venda> BuscarPorId(int id);

    Task<Venda> Adicionar(Venda venda);

    Task<Venda> Atualizar(Venda venda, int id);
  }
}