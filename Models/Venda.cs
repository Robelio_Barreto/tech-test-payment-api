namespace tech_test_payment_api.Models
{
  public class Venda
  {
    public int Id { get; set; }
    public int? VendedorId { get; set; }
    public string Itens { get; set; }
    public decimal Valor { get; set; }
    public virtual Vendedor Vendedor { get; set; }
    public DateTime DataVenda { get; set; }
    public StatusVenda status { get; set; }
  }
}