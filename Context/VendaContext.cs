using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Map;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
  public class VendaContext : DbContext
  {
    public VendaContext(DbContextOptions<VendaContext> options) : base(options)
    {

    }

    public DbSet<Venda> Vendas { get; set; }
    public DbSet<Vendedor> Vendedores { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.ApplyConfiguration(new VendaMap());
      modelBuilder.ApplyConfiguration(new VendedorMap());
      base.OnModelCreating(modelBuilder);
      modelBuilder.Entity<Venda>()
       .Property(valor => valor.Valor)
       .HasColumnType("decimal(18,2)");
    }
  }
}