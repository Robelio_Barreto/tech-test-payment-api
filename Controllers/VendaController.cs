using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repository.Interface;

namespace tech_test_payment_api.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class VendaController : ControllerBase
  {
    private readonly IVendaRepository _vendaRepository;

    public VendaController(IVendaRepository vendaRepository)
    {
      _vendaRepository = vendaRepository;
    }
    [HttpGet("lista")]
    public async Task<ActionResult<List<Venda>>> BuscarVendas()
    {
      List<Venda> vendas = await _vendaRepository.BuscarVendas();
      return Ok(vendas);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Venda>> BuscarPorId(int id)
    {
      Venda venda = await _vendaRepository.BuscarPorId(id);
      return Ok(venda);
    }

    [HttpPost("Registrar")]
    public async Task<ActionResult<Venda>> RegistrarVenda([FromBody] Venda venda)
    {
      if (venda.DataVenda == DateTime.MinValue)
        return BadRequest(new { Erro = "A data da tarefa não pode ser vazia" });

      Venda cadastro = await _vendaRepository.Adicionar(venda);
      return Ok(cadastro);
    }
    [HttpPut("Atualizar")]
    public async Task<ActionResult<Venda>> Atualizar([FromBody] Venda venda, int id)
    {
      venda.Id = id;
      Venda vendaAtual = await _vendaRepository.Atualizar(venda, id);
      return Ok(vendaAtual);
    }
  }
}