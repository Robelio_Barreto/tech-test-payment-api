using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repository.Interface;

namespace tech_test_payment_api.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class VendedorController : ControllerBase
  {
    private readonly IVendedorRepository _vendedorRepository;

    public VendedorController(IVendedorRepository vendedorRepository)
    {
      _vendedorRepository = vendedorRepository;
    }
    [HttpGet("lista")]
    public async Task<ActionResult<List<Vendedor>>> BuscarVendedores()
    {
      List<Vendedor> vendedores = await _vendedorRepository.BuscarVendedores();
      return Ok(vendedores);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Vendedor>> BuscarPorId(int id)
    {
      Vendedor vendedor = await _vendedorRepository.BuscarPorId(id);
      return Ok(vendedor);
    }

    [HttpPost("Registrar")]
    public async Task<ActionResult<Vendedor>> RegistrarVendedor([FromBody] Vendedor vendedor)
    {
      Vendedor registroVendedor = await _vendedorRepository.Adicionar(vendedor);
      return Ok(registroVendedor);
    }

    [HttpPut("{id}/Atualizar")]
    public async Task<ActionResult<Vendedor>> Atualizar([FromBody] Vendedor vendedor, int id)
    {
      vendedor.Id = id;
      Vendedor atualizarVendedor = await _vendedorRepository.Atualizar(vendedor, id);
      return Ok(atualizarVendedor);
    }
  }
}