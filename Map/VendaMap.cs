using tech_test_payment_api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace tech_test_payment_api.Map
{
  public class VendaMap : IEntityTypeConfiguration<Venda>
  {
    public void Configure(EntityTypeBuilder<Venda> builder)
    {
      builder.HasKey(x => x.Id);
      builder.Property(x => x.VendedorId);
      builder.Property(x => x.Itens).IsRequired().HasMaxLength(255);
      builder.Property(x => x.Valor).IsRequired().HasMaxLength(300);
      builder.Property(x => x.DataVenda);
      builder.Property(x => x.status).IsRequired();
      builder.HasOne(x => x.Vendedor);
    }
  }
}