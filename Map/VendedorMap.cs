using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Map
{
  public class VendedorMap : IEntityTypeConfiguration<Vendedor>
  {
    public void Configure(EntityTypeBuilder<Vendedor> builder)
    {
      builder.HasKey(x => x.Id);
      builder.Property(x => x.Nome).IsRequired().HasMaxLength(200);
      builder.Property(x => x.Email).IsRequired().HasMaxLength(200);
      builder.Property(x => x.CPF).IsRequired().HasMaxLength(50);
      builder.Property(x => x.Telefone).IsRequired().HasMaxLength(50);
    }
  }
}